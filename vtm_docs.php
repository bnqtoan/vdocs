<?php
/*
Plugin Name: VTM DOCS
Plugin URI: https://vantaymedia.vn
Description: Just another intelligence plugin
Author: Toan Bui
Version: 1.0
Author URI: http://toanbui.com
*/

require_once( 'app/boot.php' );
require_once( 'vendor/autoload.php' );

use TijsVerkoyen\CssToInlineStyles\CssToInlineStyles;
use Symfony\Component\DomCrawler\Crawler;



define( 'VTMDOCS_ROOT_DIR', __DIR__ );
define( 'VTMDOCS_ROOT_URL', plugins_url( '', __FILE__ ) );

class vtm_docs {
	protected $template_engine, $data;

	function __construct() {
		add_action( 'post_submitbox_start', array( $this, 'wpse248883_new_button' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'add_admin_assets' ) );
		add_action( 'wp_ajax_vtm_doc_api', array( $this, 'api' ) );
		add_action( 'wp_ajax_nopriv_vtm_doc_api', array( $this, 'api' ) );

		// add_action( 'admin_footer', array( $this, 'add_admin_stuff' ) ); // Deactivate for now

		$loader                = new Twig_Loader_Filesystem( VTMDOCS_ROOT_DIR . '/templates/' );
		$this->template_engine = new Twig_Environment( $loader, array(
			//'cache' => VTMDOCS_ROOT_DIR.'/templates/cached',
			'cache' => false,
			'debug' => true
		) );
		$this->template_engine->addExtension( new Twig_Extension_Debug() );
		$this->template_engine->addExtension( new Twig_Extension_StringLoader() );
		$this->template_engine->addGlobal( 'rooturl', VTMDOCS_ROOT_URL . '/templates' );
		$this->template_engine->addGlobal( 'date', date( 'd/m/Y' ) );
        $this->data = [];

		$function = new Twig_Function( 'read_number', 'convert_number_to_words' );
		$func_timeline = new Twig_Function( 'timeline', 'vt_timeline' );
		$this->template_engine->addFunction( $function );
		$this->template_engine->addFunction( $func_timeline );

		add_filter( 'the_content', [ $this, 'renderContent' ] );

		add_shortcode( 'block', [ $this, 'sc_block' ] );
	}

	function add_admin_stuff() {
		?>
        <script>
            if (!window.jQuery) {
                var script = document.createElement('script');
                script.type = "text/javascript";
                script.src = "https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js";
                document.getElementsByTagName('head')[0].appendChild(script);
            }
        </script>
        <div class="admin-support">
            <div class="admin-chat-head">
                <a href="#"><img src="<?php echo VTMDOCS_ROOT_URL; ?>/assets/images/support.png" alt="support"></a>
            </div>
            <div class="admin-support-panel">
                <div class="panel-header">Tài liệu hỗ trợ dịch vụ</div>
                <div class="panel-body">
                    <div class="search-content">

                    </div>
                    <form action="" class="search-form">
                        <input type="text" placeholder="Tìm kiếm trong KB" class="acf-field input" name="search"/>
                    </form>
                </div>
                <div class="panel-footer">
                    <a class="button button-primary btn-chat" data-cmd="show.index" href="#">Xem mục lục</a>
                </div>
            </div>
        </div>
		<?php
	}

	function get_content( $post_id, $inline = false ) {
		remove_filter( 'the_content', [ $this, 'renderContent' ] );

		$content = get_post_field( 'post_content', $post_id );
		$content = apply_filters( 'the_content', $content );
		$content = do_shortcode( $content );

		$css      = get_post_meta( $post_id, '_wpb_shortcodes_custom_css', true );
		$post_css = get_post_meta( $post_id, '_wpb_post_custom_css', true );

		if ( $inline && ! empty( $content ) ) {
			$template = $this->template_engine->createTemplate( $content );
			$content  = $template->render( $this->data );
			$cssToInlineStyles = new CssToInlineStyles();
			$content           = $cssToInlineStyles->convert( $content, "$css\n$post_css" );
		} else {
			$content = "<style class='custom-css-$post_id'>$css\n$post_css</style>" . $content;
		}

		return $content;
	}

	static function inlineFont(){
	    $text = <<<CONTENT
            <style>
                html, body{font-family: 'Roboto', sans-serif;}
            </style>
CONTENT;
        return $text;
    }

	function get_header( $template_id ) {
		return '<header id="_print-header" class="_just-for-print">' .self::inlineFont(). $this->get_content( get_field( 'header', $template_id ), true ) . '</header>';
	}

	function get_footer( $template_id ) {
		return '<footer id="_print-footer" class="_just-for-print">'.self::inlineFont().'<div class="footer-wrap">' . $this->get_content( get_field( 'footer', $template_id ), true ) . '</div></footer>';
	}

	static function get_the_template( $doc_id = null ) {
		if ( $doc_id == null ) {
			global $post;
			$doc_id = $post->ID;
		}
		$template_id = get_field( 'template', $doc_id, true );
		if ( $template_id && is_array( $template_id ) && isset( $template_id[0] ) ) {
			return $template_id[0];
		}

		return false;
	}

	function sc_block( $atts, $content ) {
		$name          = $atts['name'];
		$blocks        = get_field( 'blocks', self::get_the_template() );
		$block_content = '';
		if ( is_array( $blocks ) ) {
			foreach ( $blocks as $block ) {
				if ( $block['ten'] === $name ) {
					$block_content = $block['noi_dung'];
				}
			}
		}

		return $block_content;
	}

	/**
	 * Just testing right now
	 */
	function api() {
		$id = $_POST['id'];

		if ( 'doc' === get_post_type( $id ) ) {
			$doc_url = get_the_permalink( $id );
            $url = "http://v-api.vantaymedia.com:5000/pdf/$doc_url?format=A4&top=4cm&left=2cm&bottom=4cm&right=2cm&timeout=0&displayHeaderFooter=true";
			$dest    = wp_get_upload_dir() . '/docs/' . $id . '.pdf';
			copy($url,$dest);
			update_post_meta( $id, '_pdf_url', $url );
			echo $url;
		}

		$html = file_get_contents(VTMDOCS_ROOT_DIR.'/Sheet1.html');

		$crawler = new Crawler($html);

		$crawler->filter('style, link, meta, head, .row-header, .row-headers-background, script, thead')->each(function (Crawler $crawler){
			foreach ($crawler as $node) {
				$node->parentNode->removeChild($node);
			}
		});

		$crawler->filter('table')->each(function (Crawler $crawler){
			foreach ($crawler as $node) {
                $node->setAttribute('class', 'table table-bordered');
			}
		});

		$crawler->filter('td, tr')->each(function (Crawler $crawler){
			foreach ($crawler as $node) {
                $node->removeAttribute('class');
                $node->removeAttribute('style');
			}
		});

		echo $crawler->filter('.grid-container')->html();


		exit();
	}

	function renderContent( $content ) {
		if ( in_array(get_post_type(),['doc','doc-blank']) ) {
			$template_id      = self::get_the_template();
			$template_content = $this->get_content( $template_id );

			$data = get_fields();


			$raw_data = json_decode(get_field('raw_data'), true);
			if ($raw_data){
				$data = array_merge($data, $raw_data);
			}

            $this->data = $data;

			$this->template_engine->addGlobal( 'ma_so', get_post_meta(get_the_ID(), '_doc_id', true) );
			$this->template_engine->addGlobal( 'title', get_the_title() );

			$template_content = $template_content . $this->get_header( $template_id );
			$template_content = $template_content . $this->get_footer( $template_id );

			$template = $this->template_engine->createTemplate( $template_content );

			//solid_log(json_decode(get_field('raw_data'), true));



			foreach ( $data as $key => $item ) {
				if ( empty( $item ) ) {
					unset( $data[ $key ] );
				}
			}

			$content = $template->render( $data );
		}

		return $content;
	}

	function render() {
		$template      = $this->template_engine->load( 'invoices/letweb/index.html' );
		$data          = get_fields( 10 );
		$data['_meta'] = [
			'date'         => date( 'd/m/Y' ),
			'project_name' => get_the_title( 10 )
		];
		echo $template->render( $data );
	}

	function add_admin_assets() {
		wp_register_script( 'vtm-docs', VTMDOCS_ROOT_URL . '/assets/js/vtm-docs-admin.js', [ 'jquery' ] );
		wp_register_style( 'vtm-docs-admin', VTMDOCS_ROOT_URL . '/assets/css/admin.css' );
		wp_localize_script( 'vtm-docs', 'ADMIN_CLIENT', [
			'ajax_url' => admin_url( 'admin-ajax.php?action=vtm_doc_api' )
		] );
		wp_enqueue_style( 'vtm-docs-admin' );
		wp_enqueue_script( 'vtm-docs' );
	}

	function add_create_doc_button() {
		if ( 'doc' === get_post_type() ) {
			?>
            <div class="publishing-action">
                <p>Chú ý: vui lòng lưu lại tài liệu trước khi sinh file pdf, <b style="color: red">nếu không, dữ liệu
                        bạn chưa lưu sẽ bị mất</b></p>
                <input data-id="<?php the_ID(); ?>" name="save" type="button"
                       class="create-docs button-large button-primary" value="Create Docs"/>
				<?php
				$pdf_url = get_post_meta( get_the_ID(), '_pdf_url', true );
				if ( $pdf_url ) {
                    ?>
                    <a class="button-large button-primary" target="_blank" href="<?php echo $pdf_url; ?>"> Download </a>
                    <?php
				} ?>
            </div>
			<?php
		}
	}
}

add_action( 'init', function () {
    if (is_plugin_active('js_composer/js_composer.php')){
	    $docs = new vtm_docs();
    }
} );