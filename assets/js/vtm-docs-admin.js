(function ($) {
    $(document).ready(function(){
        function sayTyping(){
            var loading = '<div class="loading-text">Đang tìm, chờ chút nha...</div>';
            $('.search-content').html(loading);
        }

        function sendMessage(submit_data){
            $.ajax({
                url: ADMIN_CLIENT.ajax_url,
                type: 'post',
                data: submit_data,
                beforeSend: function () {
                    sayTyping();
                },
                success: function (data) {
                    $('.search-content').html(data);
                }
            });
        }

        $('.create-docs').click(function (e) {
            e.preventDefault();
            var _self = $(this);
            var id = _self.data('id');
            $.ajax({
               url: ADMIN_CLIENT.ajax_url,
                type: 'post',
                data:{
                   id: id
                }, success: function (data) {
                    location.reload();
                }
            });
        });

        $('.search-form').on('submit', function () {
            var _self = $(this);
            var submit_data = _self.serialize();
            sendMessage(submit_data);
            return false;
        });

        $('.btn-chat').on('click', function (e) {
            e.preventDefault();
            var submitData = {};
            submitData.search = $(this).data('cmd');
            sendMessage(submitData);
        });
    });
})(jQuery);