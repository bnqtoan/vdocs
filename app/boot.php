<?php
require_once( 'functions.php' );
require_once( 'post-types/header.php' );
require_once( 'post-types/template.php' );
require_once( 'post-types/footer.php' );
require_once( 'post-types/doc.php' );
require_once( 'post-types/doc-blank.php' );

function vt_set_doc_id_on_save( $post_id ) {
	if ('doc' != get_post_type()) return;
	if ( $parent_id = wp_is_post_revision( $post_id ) ) {
		$post_id = $parent_id;
	}
	remove_action( 'save_post', 'vt_set_doc_id_on_save' );
	vt_set_doc_id( $post_id, vt_get_doc_id( get_field( 'loai_van_ban' ), get_field( 'dich_vu' ), get_field( 'nhanh' ) ) );
	add_action( 'save_post', 'vt_set_doc_id_on_save' );
}

add_action( 'save_post', 'vt_set_doc_id_on_save' );
add_action( 'add_meta_boxes', 'vt_register_meta_boxes_for_doc' );