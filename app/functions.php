<?php
use Mexitek\PHPColors\Color;

if ( ! function_exists( 'solid_log' ) ) {
	function solid_log( $data, $name = null, $mode = 'append' ) {
		if ( $name == null ) {
			$name = date( 'YmdH' ) . '.txt';
		}
		$dir = VTMDOCS_ROOT_DIR . "/logs";

		if ( ! is_dir( $dir ) ) {
			mkdir( $dir, 0775, true );
		}

		$filename = "$dir/$name";
		if ( is_array( $data ) || is_object( $data ) ) {
			$data = serialize( $data );
		}
		$mode = $mode == 'append' ? FILE_APPEND : null;

		$data = date( "d-m-Y H:i:s " ) . $data;
		if ( $mode == FILE_APPEND ) {
			$data .= "\n";
		}

		return file_put_contents( $filename, $data, $mode );
	}
}


function convert_number_to_words( $number ) {
	$hyphen      = ' ';
	$conjunction = '  ';
	$separator   = ' ';
	$negative    = 'âm ';
	$decimal     = ' phẩy ';
	$dictionary  = array(
		0                   => 'Không',
		1                   => 'Một',
		2                   => 'Hai',
		3                   => 'Ba',
		4                   => 'Bốn',
		5                   => 'Năm',
		6                   => 'Sáu',
		7                   => 'Bảy',
		8                   => 'Tám',
		9                   => 'Chín',
		10                  => 'Mười',
		11                  => 'Mười một',
		12                  => 'Mười hai',
		13                  => 'Mười ba',
		14                  => 'Mười bốn',
		15                  => 'Mười năm',
		16                  => 'Mười sáu',
		17                  => 'Mười bảy',
		18                  => 'Mười tám',
		19                  => 'Mười chín',
		20                  => 'Hai mươi',
		30                  => 'Ba mươi',
		40                  => 'Bốn mươi',
		50                  => 'Năm mươi',
		60                  => 'Sáu mươi',
		70                  => 'Bảy mươi',
		80                  => 'Tám mươi',
		90                  => 'Chín mươi',
		100                 => 'trăm',
		1000                => 'ngàn',
		1000000             => 'triệu',
		1000000000          => 'tỷ',
		1000000000000       => 'nghìn tỷ',
		1000000000000000    => 'ngàn triệu triệu',
		1000000000000000000 => 'tỷ tỷ'
	);

	if ( ! is_numeric( $number ) ) {
		return false;
	}

	if ( ( $number >= 0 && (int) $number < 0 ) || (int) $number < 0 - PHP_INT_MAX ) {
		// overflow
		trigger_error( 'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX, E_USER_WARNING );

		return false;
	}

	if ( $number < 0 ) {
		return $negative . convert_number_to_words( abs( $number ) );
	}

	$string = $fraction = null;

	if ( strpos( $number, '.' ) !== false ) {
		list( $number, $fraction ) = explode( '.', $number );
	}

	switch ( true ) {
		case $number < 21:
			$string = $dictionary[ $number ];
			break;
		case $number < 100:
			$tens   = ( (int) ( $number / 10 ) ) * 10;
			$units  = $number % 10;
			$string = $dictionary[ $tens ];
			if ( $units ) {
				$string .= $hyphen . $dictionary[ $units ];
			}
			break;
		case $number < 1000:
			$hundreds  = $number / 100;
			$remainder = $number % 100;
			$string    = $dictionary[ $hundreds ] . ' ' . $dictionary[100];
			if ( $remainder ) {
				$string .= $conjunction . convert_number_to_words( $remainder );
			}
			break;
		default:
			$baseUnit     = pow( 1000, floor( log( $number, 1000 ) ) );
			$numBaseUnits = (int) ( $number / $baseUnit );
			$remainder    = $number % $baseUnit;
			$string       = convert_number_to_words( $numBaseUnits ) . ' ' . $dictionary[ $baseUnit ];
			if ( $remainder ) {
				$string .= $remainder < 100 ? $conjunction : $separator;
				$string .= convert_number_to_words( $remainder );
			}
			break;
	}

	if ( null !== $fraction && is_numeric( $fraction ) ) {
		$string .= $decimal;
		$words  = array();
		foreach ( str_split( (string) $fraction ) as $number ) {
			$words[] = $dictionary[ $number ];
		}
		$string .= implode( ' ', $words );
	}

	return $string;
}

function vt_timeline( $data_txt ) {
    ob_start();
	$wrap_span = 10;
	$to_wrap   = 10;
	$data_txt = explode( "\n", $data_txt );
	$values   = [];
	$format_map = [
		"Chung" => 1,
		"Design" => 2,
		"Coding" => 3
	];
	foreach ( $data_txt as $tr ) {
		$tr = explode( "\t", $tr );
		array_push( $values, $tr );
	}

	$format = [];

	foreach ($values as $i=>&$tr){
	    foreach ($tr as $j => $td){
	        if ($j == 0){
		        array_push($format, $td);
		        unset($tr[$j]);
		        $tr = array_values($tr);
            }
        }
    }

    //print_r($format);

	foreach ($values as $i=>&$tr){
		foreach ($tr as $j => &$td){
		    $td = trim($td);
			if ($i > 0 && $j > 0 && $td !== '1') $td = '';
		}
	}


	$max    = count( $values[0] );
	//$colors = "#6D439A #26B67C #FCB416 #1b1b1b #420039 #CDCDCD #EAD7D1 #EC7505 #2E2532 #81D2C7 #484041 #FFF275";
	$colors = "#694991 #00bb7e #ffb511 #006ebc";
	$colors = explode( ' ', $colors );
	//shuffle( $colors );
	$wrapmode = $max > $to_wrap;
	$range    = $wrapmode ? ceil( ( $max - 1 ) / $wrap_span ) : $max;
	//print_r($values);
	?>
    <div class="timeline-wrap">
		<?php
		$w       = 70 / $max;
		for ($i = 0; $i< count($values); $i++){
		    $__tr = $values[$i];
		    //$tr = $values[$i];
			?>
            <div class="timeline-tr<?php echo $i == 0 ? ' timeline-heading' : ''; ?>">
				<?php
				$length = 0;
				$offset = 0;
				$touch = false;
				foreach ( $__tr as $j => $td ) {
				    if ($j > 0){
					    if (empty( $td )) {
						    if (!$touch){
							    $offset ++;
						    }
					    } else {
						    $length ++;
					    }
					    $touch = $length > 0;
                    }
				}
				?>
				<?php if ( $i === 0 ) {
					foreach ( $__tr as $j => $td ) {
						if ( $j > 0 && ( $j - 1 ) % $wrap_span === 0 ) {
							$current_range = ceil( $j / $wrap_span );
							$num           = $current_range == $range ? $max - 1 : $__tr[ $j ] + $wrap_span - 1;
							$this_w = $num >= $wrap_span * $current_range ? $wrap_span : ( $max - 1 ) % $wrap_span;
							$this_w = $this_w * $w;
							?>
                            <div data-length="<?php echo $this_w; ?>" style="width: <?php echo $this_w; ?>%"
                                 class="timeline-td">
								<?php echo $num; ?>
                            </div>
							<?php
						} else {
							if ( $j === 0 ) {
								?>
                                <div style="width: 26%" class="timeline-td"></div>
								<?php
							}
						}
					}
				} else {

				    //echo $colors[ $format_map[$format[$i]] ];
					$bg    = new Color( $colors[ $format_map[$format[$i]] ] );
					$color = '#fff';
					if ( $bg->isLight() ) {
						$color = '#1b1b1b';
					}

					?>
                    <div style="width: 26%" class="timeline-td a-1-1">
                        <?php echo $__tr[0]; ?>
                    </div>
                    <div class="timeline-td timeline-body" data-abc="<?php echo $i; ?>"
                         style="color: <?php echo $color; ?>;background-color: <?php echo $bg; ?>;width: <?php echo $length * $w; ?>%; margin-left: <?php echo $offset * $w; ?>%">
	                    <?php
                            if ($length > 0){
                                ?>
                                <span class="timeline-left"><?php echo str_pad( min($offset + 1, $max), 2, 0, STR_PAD_LEFT ); ?></span>
	                            <?php echo $length; ?>
                                <span class="timeline-right"><?php echo str_pad( $offset + $length, 2, 0, STR_PAD_LEFT ); ?></span>
                                <?php
                            }
                        ?>
                    </div>
					<?php
				} ?>
            </div>
			<?php
		} ?>
        <div style="margin-top: 1em; text-align: right;">
            <p>Tổng thời gian: <?php echo $max - 1; ?> ngày làm việc</p>
            <p><i>Ngày làm việc: không kể thứ 7, CN và các ngày nghỉ lễ theo quy định của nhà nước.</i></p>
        </div>
    </div>
	<?php
    return ob_get_clean();
}


function get_last_document_id(){

}

function vt_get_doc_id($loai_van_ban, $dich_vu, $nhanh){
    $option_name = md5("{$loai_van_ban}{$dich_vu}{$nhanh}");
    $option_value = get_option($option_name, 0);
	$option_value++;

	$year = date('Y');
	$format = "{$nhanh}/{$loai_van_ban}/{$year}/%'.04d";
    //get_field( 'id', 'option' )[2]['format']
	$id = sprintf($format , $option_value );
	update_option($option_name, $option_value);
	return $id;
}

function vt_set_doc_id($post_id, $id){
    if (empty(get_post_meta($post_id, '_doc_id', true)) && 'publish' !== get_post_status())
        update_post_meta($post_id, '_doc_id', $id);
}

function vt_register_meta_boxes_for_doc() {
	add_meta_box( 'meta-box-id', __( 'Thông tin tài liệu', 'textdomain' ), 'vt_doc_meta_box_display', ['doc' , 'doc-blank' ], 'side', 0);
}

function vt_doc_meta_box_display(){
    ?>
    <ul>
        <li>
            ID: <strong><?php echo get_post_meta(get_the_ID(), '_doc_id', true) ?></strong>
        </li>
    </ul>
    <?php
}